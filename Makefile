# 2019 John White <john@mc128k.com>

GOCMD=go
GOBUILD=$(GOCMD) build
GOGEN=$(GOCMD) generate
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
NODEBUGSYM=-ldflags="-w -s"

MOD_PKG=mc128k.com/autofixnames
EXEC_NAME=autofixnames

ifeq ($(OS),Windows_NT)
    OS=windows
else
    UNAME_S := $(shell uname -s)
    ifeq ($(UNAME_S),Linux)
        OS=linux
    endif
    ifeq ($(UNAME_S),Darwin)
        OS=macos
    endif
endif

all: clean build

clean:
	rm -Rf bin/*

build: build_darwin-amd64 build_linux-amd64

build_darwin-amd64:
	mkdir -p bin/darwin
	env GOOS=darwin GOARCH=amd64 $(GOBUILD) $(NODEBUGSYM) -o "bin/linux/$(EXEC_NAME)" $(MOG_PKG)
	chmod +x "bin/darwin/$(EXEC_NAME)"

build_linux-amd64:
	mkdir -p bin/linux
	env GOOS=linux GOARCH=amd64 $(GOBUILD) $(NODEBUGSYM) -o "bin/linux/$(EXEC_NAME)" $(MOG_PKG)
	chmod +x "bin/linux/$(EXEC_NAME)"

install:
	cp -f bin/autofixnames /usr/local/bin/

uninstall:
	rm /usr/local/bin/autofixnames

