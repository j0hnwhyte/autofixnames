// 2019 John White <john@mc128k.com>
package main

import (
	"fmt"
	"io/fs"
	"math/rand"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"syscall"
	"time"

	KP "gopkg.in/alecthomas/kingpin.v2"
)

const version = "v0.0.3"

var (
	app            = KP.New("autofixnames", "Search recursively for files and folders with invalid names (for windows/mac interoperability), performing basic naming fixes where necessary, in order to improve compatibility across systems.")
	workingDir     = app.Arg("working directory", "Scan files and directory from this path.").Default(".").String()
	fixFlag        = app.Flag("fix", "Change the file names with the fixed ones (WARNING: Make a backup first).").Short('f').Bool()
	checkLongNames = app.Flag("longNames", "Check files with long names").Short('l').Bool()
)

func init() {
	app.HelpFlag.Short('h')
	app.Version(version).VersionFlag.Short('V')
	rand.Seed(time.Now().UnixNano())
}
func main() {
	KP.MustParse(app.Parse(os.Args[1:]))
	fixFrom := make([]string, 0)
	fixTo := make([]string, 0)

	if *workingDir == "" {
		app.FatalUsage("Missing working directory")
	}
	wd, err := filepath.Abs(filepath.Clean(*workingDir))
	if err != nil {
		panic(err)
	}

	encounteredPossibleMacUtfError := false

	// Walk all files and directories
	err = filepath.Walk(wd, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			if e, ok := err.(*fs.PathError); ok {
				if e.Err == syscall.ENOENT {
					// We might have encountered a UTF encoding error on macOS, or the file disappeared.
					// This happens because the accented character is encoded with two entities rather than one. macOS doesn't like it
					// The filesystem replaces umlauts with the normal letter followed by a diaeresis marker, but it's not how
					// the file is named in the SMB/AFP server
					if runtime.GOOS == "darwin" {
						fmt.Printf("❌  %s: file disappeared or contains UTF characters not supported by macOS\n", path)
						encounteredPossibleMacUtfError = true
					} else {
						return err
					}
				} else if e.Err == syscall.EACCES {
					fmt.Printf("❌  %s: permission denied (EACCESS)\n", path)
				} else {
					return err
				}
			} else {
				return err
			}
		}
		if info.Name() == "." {
			return nil
		}

		fix, err := checkFile(path, info)
		if err != nil {
			return err
		}

		// Does the file need fixing?
		if fix != info.Name() {
			fixFrom = append(fixFrom, path)
			fixTo = append(fixTo, filepath.Dir(path)+string(os.PathSeparator)+fix) // Do not clean bc of spaces
		}

		return nil
	})
	if encounteredPossibleMacUtfError {
		fmt.Println("\nWe may have encountered an UTF encoding error. This happens because the")
		fmt.Println("filesystem can't display normal letters followed by a UTF diaeresis marker")
		fmt.Println("(like COMBINING GRAVE ACCENT). This happens when the file name is formatted as")
		fmt.Println("Unicode NFD (normal form decomposed, that is accents are encoded as two")
		fmt.Println("characters) while accessing it via SMB or AFP. ")
		fmt.Println("\nTry running the following command on the (linux) fileserver:\n")
		fmt.Println("$ convmv -r -f UTF-8 -t UTF-8 --nfc --notest /my/folder")
	}
	if err != nil {
		panic(err)
	}

	// We perform the fixes last because we need to do them in inverted order, or we risk renaming a parent
	// without first renaming its children
	if *fixFlag {
		ln := len(fixFrom)
		for i := ln - 1; i >= 0; i-- {
			from := fixFrom[i]
			to := fixTo[i]

			err := fixFile(from, to)
			if err != nil {
				fmt.Printf("💩  %s\n", err.Error())
			}
		}
	}

	return
}

// fixList is sorted by path depth, so the first elements are the parents
// We return the fixed file name (excluding the path) for applying corrections, or the same file name if
// no corrections are required
func checkFile(path string, info os.FileInfo) (fixedName string, err error) {
	name := info.Name()

	// https://docs.microsoft.com/en-us/windows/win32/fileio/naming-a-file

	// Must not contain veto characters
	// The following reserved characters for windows: <>:"/\|?*
	fixedName1 := name
	fixedName1 = strings.ReplaceAll(fixedName1, "<", "")
	fixedName1 = strings.ReplaceAll(fixedName1, ">", "")
	fixedName1 = strings.ReplaceAll(fixedName1, ":", "-")
	fixedName1 = strings.ReplaceAll(fixedName1, "\"", "'")
	fixedName1 = strings.ReplaceAll(fixedName1, "\\", "-")
	fixedName1 = strings.ReplaceAll(fixedName1, "|", "-")
	fixedName1 = strings.ReplaceAll(fixedName1, "?", "")
	fixedName1 = strings.ReplaceAll(fixedName1, "*", "")
	//fixedName = strings.ReplaceAll(fixedName, "&", "") // never seen it giving problems, can be escaped, useful
	if fixedName1 != name {
		fmt.Printf("❕  %s: name contains characters forbidden by specific OS: <>:\"\\|?*\n", path)
	}

	// My overzealous rules
	fixedName2 := fixedName1
	fixedName2 = strings.ReplaceAll(fixedName2, "`", "")
	fixedName2 = strings.ReplaceAll(fixedName2, ";", "")
	//fixedName2 = strings.ReplaceAll(fixedName2, "'", "")
	if fixedName2 != fixedName1 {
		fmt.Printf("❕  %s: name contains uncommon characters: `;\n", path)
	}

	// Must not begin or end with a space
	fixedName3 := fixedName2
	fixedName3 = strings.TrimSpace(fixedName3)
	if fixedName3 != fixedName2 {
		fmt.Printf("❕  %s: name starts or ends with whitespace\n", path)
	}

	// Must not end with a dot FIXME: multiple?
	fixedName4 := fixedName3
	fixedName4 = strings.TrimSuffix(fixedName4, ".")
	fixedName4 = strings.TrimSpace(fixedName4)
	if fixedName4 != fixedName3 {
		fmt.Printf("❕  %s: name ends with a dot\n", path)
	}

	// Unicode text must be normalized to NFC (e.g.:  è -> è)
	//buf := make([]byte, len(fixedName4)+50)
	//t := transform.Chain(norm.NFC)
	//n, _, err := t.Transform(buf, []byte(fixedName4), true)
	//if err != nil {
	//	panic(err)
	//}
	//fixedName5 := string(buf[:n])
	//if fixedName5 != fixedName4 {
	//	fmt.Printf("❕  %s: unicode text not normalized to NFC\n", path)
	//}
	// For some reason go gives us names encoded as NFD, so we can't properly detect this?

	fixedName = fixedName4

	// Name length must be < 63
	if *checkLongNames {
		if len(fixedName) >= 63 {
			fmt.Printf("❌  %s: file name longer than 63 characters (can't fix)\n", path)
		}
	}

	// Path length must be < 256 (excluding drive identifier)
	if len(filepath.Join(filepath.Dir(path), fixedName)) >= 255 {
		fmt.Printf("❌  %s: full path longer than 255 characters (can't fix)\n", path)
	}

	return
}

func fixFile(from, to string) error {
	// Check if something exists already
	_, err := os.Stat(to)
	if os.IsNotExist(err) {
		// Okay
	} else {
		// If name collides, trim and append a random number
		to = to + " " + strconv.Itoa(rand.Intn(9999-1000+1)+1000)
		_, err = os.Stat(to)
		if os.IsNotExist(err) {
			// Okay
		} else {
			return err
		}
	}

	err = os.Rename(from, to)
	if err != nil {
		return err
	}

	fmt.Printf("✅  %s: fixed\n", to)

	return nil
}

// TODO: ignore appledouble
